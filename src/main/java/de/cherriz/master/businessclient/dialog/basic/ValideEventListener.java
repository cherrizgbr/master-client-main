package de.cherriz.master.businessclient.dialog.basic;

/**
 * Schnittstelle für die Listener von UI Events die bereits durch die UI validiert wurden.
 *
 * @author Frederik Kirsch
 */
public  interface  ValideEventListener <E extends ValideEventType> {

    /**
     * Ein valides UI Event wurde ausgeloesst.
     *
     * @param type Der Eventtyp.
     */
    void valideEvent(E type);

}