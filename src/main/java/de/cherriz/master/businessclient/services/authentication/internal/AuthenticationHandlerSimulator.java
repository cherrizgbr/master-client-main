package de.cherriz.master.businessclient.services.authentication.internal;

import de.cherriz.master.businessclient.services.authentication.AuthenticationHandler;
import de.cherriz.master.businessclient.shared.User;

/**
 * Dummyimplementierung für die Authentifizierung von Logins.
 *
 * @author Frederik Kirsch
 */
public class AuthenticationHandlerSimulator implements AuthenticationHandler {

    private String testUser = null;

    private String testPassword = null;

    public AuthenticationHandlerSimulator(String testUser, String testPassword) {
        this.testUser = testUser;
        this.testPassword = testPassword;
    }

    @Override
    public User authenticate(User user) {
        if (user.getUser().equals(this.testUser) && user.getPassword().equals(this.testPassword)) {
            user.setLoggedIn();
        }
        return user;
    }

}