package de.cherriz.master.businessclient.services.auftrag;

/**
 * Service für den Zugriff auf den REST Service für Auftraege.
 *
 * @author Frederik Kirsch
 */
public interface AuftragService {

    /**
     * Laedt einen Auftrag zu einer AuftragsID.
     *
     * @param auftragID Die ID des Auftrags.
     * @return Der Auftrag.
     */
    Auftrag getProcessAttributes(Long auftragID);

}