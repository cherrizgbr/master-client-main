package de.cherriz.master.businessclient.dialog;

/**
 * Schnittstelle für die View im Kontext des MVVM Patterns.
 *
 * @author Frederik Kirsch
 */
public interface View {

    /**
     * Setzt das ViewModell, welches an die Properties der UI gebunden wird.
     *
     * @param model Das ViewModel.
     */
    public void setViewModel(ViewModel model);

}