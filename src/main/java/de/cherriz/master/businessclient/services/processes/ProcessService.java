package de.cherriz.master.businessclient.services.processes;

import java.util.List;
import java.util.Map;

/**
 * Service für den Zugriff auf den REST Service für Prozesse.
 *
 * @author Frederik Kirsch
 */
public interface ProcessService {

    /**
     * Liefert aktive Prozessinstanzen die an definierten Prozesschritten stehen.
     *
     * @param processSteps Die Prozessschritte.
     * @return Die aktiven Prozesse.
     */
    List<ProcessInstance> getProcessInstances(List<StepDescription> processSteps);

    /**
     * Liefert die Attribute zu einer ProzessID.
     *
     * @param processID Die ProzessID.
     * @return Die Attribute.
     */
    Map<String, Object> getProcessAttributes(String processID);

    /**
     * Setzt das definierte Attribut am definierten Prozess.
     *
     * @param processID Die ID der Prozessinstanz.
     * @param attribute Das Attribut.
     * @param value     Der Attributwert.
     */
    void setProcessAttribute(String processID, String attribute, String value);

    /**
     * Beendet die Bearbeitung eines Prozessschrittes einer spezifischen Prozessinstanz.
     *
     * @param processID Die Prozessinstanz.
     */
    void dispatchProcess(String processID);

}