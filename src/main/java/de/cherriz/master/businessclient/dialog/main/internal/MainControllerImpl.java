package de.cherriz.master.businessclient.dialog.main.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.AutowiredDialogController;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.dialog.main.MainController;
import de.cherriz.master.businessclient.services.processes.ProcessInstance;
import de.cherriz.master.businessclient.services.processes.ProcessService;
import de.cherriz.master.businessclient.services.processes.StepDescription;
import de.cherriz.master.businessclient.shared.Plugin;
import de.cherriz.master.businessclient.shared.PluginStarter;
import de.cherriz.master.businessclient.shared.User;
import javafx.stage.Window;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Implementierung des MainController.
 *
 * @author Frederik Kirsch
 */
public class MainControllerImpl extends AutowiredDialogController implements MainController, ValideEventListener<ValidePostkorbEventType> {

    private List<Plugin> plugins = null;

    private ProcessService processService = null;

    private PluginStarter pluginStarter = null;

    /**
     * Instanziiert den Controller und setzt notwendige Parameter.
     *
     * @param fxml  Die FXML des Hauptdialogs.
     * @param name  Der Name des Dialogs.
     * @param owner Der Owner des Dialogs.
     */
    public MainControllerImpl(String fxml, String name, Window owner) {
        super(fxml, owner);
        this.setTitle(name);
        this.setMinWidth(1000);
        this.setMinHeight(600);
        this.loadContent();
    }

    /**
     * Laedt den Inhalt des Dialogs.
     */
    private void loadContent() {
        MainViewModel model = this.getViewModel();

        this.plugins = this.loadPlugins();
        model.setPlugins(this.plugins);
    }

    /**
     * Laedt die verfuegbaren Plugins.
     * Plugins muessen eine Springkonfiguration nach dem Muster <code>plugin-*.xml</code> zur verfuegung Stellen.
     * In der Konfiguration muss eine Beandefinition für die Implementierung der Klasse {@link de.cherriz.master.businessclient.shared.Plugin} definiert sein.
     *
     * @return Die gefundenen Plugins.
     */
    private List<Plugin> loadPlugins() {
        List<Plugin> plugins = new ArrayList<>();

        try {
            if (this.isDevMode()) {
                plugins.addAll(this.loadPluginsFromClasspath());
            } else {
                plugins.addAll(this.loadPluginsFromJar());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return plugins;
    }

    /**
     * Prueft ob die Anwendung innerhalb eines IDEs ausgeführt wird, oder als Jar gepackt ist.
     *
     * @return Ob im Entwicklungsmodus.
     */
    private boolean isDevMode() throws URISyntaxException {
        boolean devMode = true;
        CodeSource cs = MainControllerImpl.class.getProtectionDomain().getCodeSource();
        if (cs.getLocation().toURI().getPath().endsWith(".jar")) {
            devMode = false;
        }
        return devMode;
    }

    /**
     * Sucht innerhalb des ClassPaths nach Pluginkonfigurationen. Wird im Rahmen der Entwicklung benoetigt.
     *
     * @return Die gefundenen Plugins.
     * @throws IOException
     */
    private List<Plugin> loadPluginsFromClasspath() throws IOException {
        List<Plugin> plugins = new ArrayList<>();
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        Resource[] locations = patternResolver.getResources("classpath*:**/plugin-*.xml");

        for (Resource item : locations) {
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(item.getURI().toString());
            plugins.addAll(context.getBeansOfType(Plugin.class).values());
        }
        return plugins;
    }

    /**
     * Sucht innerhalb der Jar nach Pluginkonfigurationen.
     *
     * @return Die gefundenen Plugins.
     * @throws IOException
     */
    private List<Plugin> loadPluginsFromJar() throws IOException {
        List<Plugin> plugins = new ArrayList<>();
        CodeSource src = MainControllerImpl.class.getProtectionDomain().getCodeSource();

        if (src != null) {
            URL jar = src.getLocation();
            ZipInputStream zip = new ZipInputStream(jar.openStream());
            ZipEntry ze;

            while ((ze = zip.getNextEntry()) != null) {
                String entryName = ze.getName();
                if (entryName.startsWith("plugin-") && entryName.endsWith(".xml")) {
                    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(entryName);
                    plugins.addAll(context.getBeansOfType(Plugin.class).values());
                }
            }

        }
        return plugins;
    }

    @Override
    protected ViewModel initViewModel() {
        MainViewModel viewModel = new MainViewModel();
        viewModel.addValideEventListener(this);
        return viewModel;
    }

    /**
     * Setzt die Version der Anwendung.
     *
     * @param version Die Version.
     */
    public void setVersion(String version) {
        MainViewModel viewModel = this.getViewModel();
        viewModel.setVersion(version);
    }

    /**
     * Setzt den Buildzeitpunkt der Anwendung.
     *
     * @param build Der Buildzeitpunkt.
     */
    public void setBuild(String build) {
        MainViewModel viewModel = this.getViewModel();
        viewModel.setBuild(build);
    }

    @Override
    public void setActiveUser(User user) {
        MainViewModel viewModel = this.getViewModel();
        viewModel.setUser(user);
    }

    /**
     * Setzt den ProcessService fuer Anfragen and die ProcessEngine.
     *
     * @param service Der ProcessService.
     */
    public void setProcessService(ProcessService service) {
        this.processService = service;
    }

    /**
     * Setzt den PluginStarter. Zum Starten der Bearbeitung mit einem Plugin.
     *
     * @param pluginStarter Der PluginStarter.
     */
    public void setPluginStarter(PluginStarter pluginStarter) {
        this.pluginStarter = pluginStarter;
    }

    @Override
    public void valideEvent(ValidePostkorbEventType type) {
        switch (type) {
            case AKTUALISIEREN:
                this.refreshVorgaenge();
                break;
            case BEARBEITEN:
                MainViewModel model = this.getViewModel();
                ProcessInstance process = model.getSelectedProcess();
                this.bearbeiten(process);
                break;
        }
    }

    /**
     * Liefert das Plugin mit dem eine {@link de.cherriz.master.businessclient.services.processes.ProcessInstance} bearbeitet werden kann.
     *
     * @param prozess Die ProcessInstance.
     * @return Das passende Plugin..
     */
    private Plugin getPluginForProcess(ProcessInstance prozess) {
        for (Plugin plugin : this.plugins) {
            if (plugin.getTaskDefinitionKey().equals(prozess.getTaskDefinitionKey())) {
                return plugin;
            }
        }
        return null;
    }

    /**
     * Laedt die Vorgaenge aus der ProcessEngine, welche von den verfuegbaren Plugins unterstuetzt werden.
     */
    private void refreshVorgaenge() {
        List<StepDescription> steps = new ArrayList<>();

        this.plugins.forEach(plugin -> steps.add(new StepDescription(plugin.getProcessDefinitionKey(), plugin.getTaskDefinitionKey())));
        List<ProcessInstance> instances = this.processService.getProcessInstances(steps);

        MainViewModel model = this.getViewModel();
        model.setVorgaenge(instances);
    }

    /**
     * Startet die Bearbeitung eine {@link de.cherriz.master.businessclient.services.processes.ProcessInstance}.
     *
     * @param prozess Die zu bearbeitende {@link de.cherriz.master.businessclient.services.processes.ProcessInstance}.
     */
    private void bearbeiten(ProcessInstance prozess) {
        Plugin plugin = this.getPluginForProcess(prozess);
        this.pluginStarter.startProcessInstance(plugin, prozess, this.getViewModel());
    }

}