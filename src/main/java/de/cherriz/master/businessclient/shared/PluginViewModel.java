package de.cherriz.master.businessclient.shared;

import de.cherriz.master.businessclient.services.processes.ProcessInstance;

/**
 * Schnittstelle fuer das ViewModel eines Plugins.
 *
 * @author Frederik Kirsch
 */
public interface PluginViewModel {

    /**
     * Setzt das aktive Plugin.
     * @param plugin Das Plugin.
     */
    public void setActivePlugin(Plugin plugin);

    /**
     * Entfernt die bearbeitet Prozessinstanz.
     *
     * @param process Die Prozessinstanz.
     */
    public void removeProcess(ProcessInstance process);

}