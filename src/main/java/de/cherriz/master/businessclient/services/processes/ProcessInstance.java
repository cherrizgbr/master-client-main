package de.cherriz.master.businessclient.services.processes;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Repraesentiert eine Prozessinstanz.
 *
 * @author Frederik Kirsch
 */
@JsonIgnoreProperties({"assignee", "due", "followUp", "delegationState", "description", "executionId", "owner", "parentTaskId", "priority", "processDefinitionId"})
public class ProcessInstance {

    private String id;

    private String processInstanceId;

    private String name;

    private String created;

    private String processDefinitionKey;

    private String taskDefinitionKey;

    /**
     * @return Die ID der Prozessinstanz.
     */
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    /**
     * @return Der Erzeugungszeitpunkt der Prozessinstanz.
     */
    public String getCreated() {
        return created;
    }

    /**
     * @return Der Name der Prozessinstanz.
     */
    public String getName() {
        return name;
    }

    /**
     * @return Der ProzessdefinitionKey der Prozessinstanz.
     */
    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    /**
     * @param processInstanceId Die ProzessinstanzID des Prozesses.
     */
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    /**
     * @return Der TaskDefinitionKey des Prozesses.
     */
    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    /**
     * Setzt den TaskdefintionKey des Prozesses.
     *
     * @param taskDefinitionKey Der TaskdefinitionKey.
     */
    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    /**
     * Setzt den Namen des Prozesses.
     *
     * @param name Der Name des Prozesses.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Setzt den Erzeugungszeitpunkt des Prozesses.
     *
     * @param created Der Erzeugungszeitpunkt.
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Setzt den ProcessDefinitionKey des Prozesses.
     *
     * @param processDefinitionKey Der ProcessDefinitionKey.
     */
    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    /**
     * @return Die ID des Prozesses.
     */
    public String getId() {
        return id;
    }

    /**
     * Setzt die ID des Prozesses.
     * @param id Die ID.
     */
    public void setId(String id) {
        this.id = id;
    }

}