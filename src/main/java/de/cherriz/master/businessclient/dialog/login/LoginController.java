package de.cherriz.master.businessclient.dialog.login;

import de.cherriz.master.businessclient.dialog.DialogController;

/**
 * Schnittstelle des Logindialogs. Erlaubt das Anbinden von Listener für erfolgreiche Logins.
 *
 * @author Frederik Kirsch
 */
public interface LoginController extends DialogController {

    /**
     * Registriert den übergebenen Listener.
     * Listener werden über erfolgreiche Logins informiert.
     *
     * @param listener Ein Listener.
     */
    void addLoggedInListener(LoggedInListener listener);

}