package de.cherriz.master.businessclient.dialog.login;

import de.cherriz.master.businessclient.shared.User;

/**
 * Implementierungen werden über erfolgreiche Logins benachrichtig.
 *
 * @author Frederik Kirsch
 */
public interface LoggedInListener {

    /**
     * Login erfolgreich mit User durchgeführt.
     *
     * @param user Der erfolgreich authentifizierte User.
     */
    void loggedIn(User user);

}