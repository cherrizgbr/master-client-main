package de.cherriz.master.businessclient.services.processes;

/**
 * Die Beschreibung eines Prozessschrittes.
 *
 * @author Frederik Kirsch
 */
public class StepDescription {

    private final String processDefinitionKey;

    private final String taskDefinitionKey;

    /**
     * Konstruktor.
     *
     * @param processDefinitionKey Die ID des Prozesses.
     * @param taskDefinitionKey Die ID des Prozessschrittes.
     */
    public StepDescription(String processDefinitionKey, String taskDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        this.taskDefinitionKey = taskDefinitionKey;
    }

    /**
     * @return Der ProzessdefinitionKey.
     */
    public String getProcessDefinitionKey() {
        return this.processDefinitionKey;
    }

    /**
     * @return Der TaskdefinitionKey.
     */
    public String getTaskDefinitionKey() {
        return this.taskDefinitionKey;
    }

}