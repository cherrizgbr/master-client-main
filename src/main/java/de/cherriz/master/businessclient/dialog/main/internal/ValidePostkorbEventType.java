package de.cherriz.master.businessclient.dialog.main.internal;

import de.cherriz.master.businessclient.dialog.basic.ValideEventType;

/**
 * Definiert die Eventtypen fuer den Hauptdialog.
 *
 * @author Frederik Kirsch
 */
public enum ValidePostkorbEventType implements ValideEventType {

    /**
     * Button Aktualisieren klicked.
     */
    AKTUALISIEREN,

    /**
     * Button Bearbeiten klicked.
     */
    BEARBEITEN;

}
