package de.cherriz.master.businessclient.dialog.main.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.services.processes.ProcessInstance;
import de.cherriz.master.businessclient.shared.Plugin;
import de.cherriz.master.businessclient.shared.PluginViewModel;
import de.cherriz.master.businessclient.shared.User;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Das ViewModel für den UI Dialog.
 *
 * @author Frederik Kirsch
 */
public class MainViewModel implements ViewModel, PluginViewModel {

    private StringProperty version = new SimpleStringProperty();

    private StringProperty build = new SimpleStringProperty();

    private StringProperty user = new SimpleStringProperty();

    private ListProperty<String> plugins = new SimpleListProperty<>();

    private ListProperty<ProcessInstance> processInstances = new SimpleListProperty<>();

    private ObjectProperty<ProcessInstance> selectedProcess = new SimpleObjectProperty<>();

    private ObjectProperty<Plugin> activePlugin = new SimpleObjectProperty<>();

    private List<ValideEventListener<ValidePostkorbEventType>> listener = null;

    /**
     * Konstruktor.
     */
    public MainViewModel() {
        this.listener = new ArrayList<>();
    }

    /**
     * Setzt die Version der Anwendung. Wird an den ViewController der UI gebunden.
     *
     * @param version Die Version.
     */
    public void setVersion(String version) {
        this.version.set(version);
    }

    /**
     * Setzt den Buildzeitpunkt der Anwendung. Wird an den ViewController der UI gebunden.
     *
     * @param build Der Buildzeitpunkt.
     */
    public void setBuild(String build) {
        this.build.set(build);
    }

    /**
     * Setzt den aktuellen Anwender. Wird an den ViewController der UI gebunden.
     *
     * @param user Der User.
     */
    public void setUser(User user) {
        this.user.set(user.getUser());
    }

    /**
     * Setzt die Plugins. Wird an die an den ViewController der UI gebunden.
     *
     * @param plugins Die Plugins.
     */
    public void setPlugins(List<Plugin> plugins) {
        List<String> pNames = new ArrayList<>();
        plugins.forEach(p -> pNames.add(p.getName()));
        ObservableList<String> items = FXCollections.observableList(pNames);
        this.plugins.set(items);
    }

    @Override
    public void setActivePlugin(Plugin plugin) {
        this.activePluginProperty().set(plugin);
    }

    @Override
    public void removeProcess(ProcessInstance process) {
        this.processInstances.remove(process);
    }

    /**
     * Setzt die Vorgaenge die in der UI dargestellt werden sollen.  Wird an die an den ViewController der UI gebunden.
     *
     * @param instances Die Vorgaenge.
     */
    public void setVorgaenge(List<ProcessInstance> instances) {
        ObservableList<ProcessInstance> items = FXCollections.observableList(instances);
        this.processInstances.set(items);
    }

    /**
     * @return Die Property der Version.
     */
    public StringProperty versionProperty() {
        return this.version;
    }

    /**
     * @return Die Property des BuildTimestamps.
     */
    public StringProperty buildProperty() {
        return this.build;
    }

    /**
     * @return Die Property des Users.
     */
    public StringProperty userProperty() {
        return this.user;
    }

    /**
     * @return Die Property der registrierten Plugins.
     */
    public ListProperty<String> pluginsProperty() {
        return this.plugins;
    }

    /**
     * @return Die Property der dargestellten Vorgaenge.
     */
    public ListProperty<ProcessInstance> processInstancesProperty() {
        return this.processInstances;
    }

    /**
     * @return Die Property des selektierten Vorgangs.
     */
    public ObjectProperty<ProcessInstance> selectedProcessProperty() {
        return this.selectedProcess;
    }

    /**
     * @return Die Property des aktiven Plugins.
     */
    public ObjectProperty<Plugin> activePluginProperty() {
        return this.activePlugin;
    }

    /**
     * Registriert den uebergebenen Eventlistener.
     * Der Listener wird ueber Events in der UI informiert.
     *
     * @param listener Der Listener.
     */
    public void addValideEventListener(ValideEventListener<ValidePostkorbEventType> listener) {
        this.listener.add(listener);
    }

    /**
     * Informiert die Listener ueber das Klicken des Aktualisieren Buttons.
     */
    public void aktualisieren() {
        this.listener.forEach(listener -> listener.valideEvent(ValidePostkorbEventType.AKTUALISIEREN));
    }

    /**
     * Informiert die Listener ueber das Klciken des Buttons Bearbeiten.
     */
    public void bearbeiten() {
        this.listener.forEach(listener -> listener.valideEvent(ValidePostkorbEventType.BEARBEITEN));
    }

    /**
     * Liefert den selektierten Prozess zurueck.
     *
     * @return Der selektierte Prozess.
     */
    public ProcessInstance getSelectedProcess() {
        return this.selectedProcess.getValue();
    }

}