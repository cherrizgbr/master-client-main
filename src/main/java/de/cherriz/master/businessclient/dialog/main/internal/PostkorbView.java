package de.cherriz.master.businessclient.dialog.main.internal;

import de.cherriz.master.businessclient.services.processes.ProcessInstance;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;


/**
 * Created by Fredi on 14.08.2014.
 */
public class PostkorbView {

    @FXML
    private TableView<ProcessInstance> tblVorgaenge;

    @FXML
    private Button btnAktualisieren;

    @FXML
    private Button btnBearbeiten;

    @FXML
    private TableColumn<ProcessInstance, String> clmId;

    @FXML
    private TableColumn<ProcessInstance, String> clmProzess;

    @FXML
    private TableColumn<ProcessInstance, String> clmDatum;

    public TableView<ProcessInstance> getTblVorgaenge() {
        return tblVorgaenge;
    }

    public Button getBtnAktualisieren() {
        return btnAktualisieren;
    }

    public Button getBtnBearbeiten() {
        return btnBearbeiten;
    }

    public TableColumn<ProcessInstance, String> getClmId() {
        return clmId;
    }

    public TableColumn<ProcessInstance, String> getClmProzess() {
        return clmProzess;
    }

    public TableColumn<ProcessInstance, String> getClmDatum() {
        return clmDatum;
    }

}
