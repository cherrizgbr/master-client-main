package de.cherriz.master.businessclient.dialog;

/**
 * Schnittstelle für alle DialogController.
 *
 * @author Frederik Kirsch
 */
public interface DialogController {

    /**
     * Öffnet den Dialog.
     */
    void show();

    /**
     * Schliesse den Dialog.
     */
    void hide();

}