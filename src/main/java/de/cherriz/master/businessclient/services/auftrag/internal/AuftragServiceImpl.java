package de.cherriz.master.businessclient.services.auftrag.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.businessclient.services.auftrag.Auftrag;
import de.cherriz.master.businessclient.services.auftrag.AuftragService;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.businessclient.services.auftrag.AuftragService}.
 *
 * @author Frederik Kirsch
 */
public class AuftragServiceImpl extends AbstractService implements AuftragService {

    @Override
    public Auftrag getProcessAttributes(Long auftragID) {
        //Request absetzen
        String result = this.getTarget(auftragID.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(result, Auftrag.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}