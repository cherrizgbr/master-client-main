package de.cherriz.master.businessclient.shared;

/**
 * Schnittstelle die auf die Bearbeitungsergebnisse eines Plugins hoert.
 *
 * @author Frederik Kirsch
 */
public interface PluginListener {

    /**
     * Bearbeitung wurde abgebrochen.
     */
    void editingCanceled();

    /**
     * Bearbeitung wird mit definiertem Rueckgabewer abgeschlossen.
     *
     * @param result Ergebnis der Bearbeitung.
     */
    void editingFinished(String result);

}