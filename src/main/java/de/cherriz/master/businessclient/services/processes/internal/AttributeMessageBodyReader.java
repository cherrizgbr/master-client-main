package de.cherriz.master.businessclient.services.processes.internal;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Konvertiert JSON in eine Liste mit Attributen und Werten.
 *
 * @author Frederik Kirsch
 */
public class AttributeMessageBodyReader implements MessageBodyReader<Map<String, Object>> {

    @Override
    public boolean isReadable(Class<?> type, Type genericType,
                              Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public Map<String, Object> readFrom(Class<Map<String, Object>> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
        // Verarbeitung vorberieten.
        Map<String, Object> result = new HashMap<>();
        JsonObject jsonObject = Json.createReader(entityStream).readObject();
        Set<String> keys = jsonObject.keySet();

        //Einzelne Attribute auswerten.
        for (String key : keys) {
            JsonObject attribute = jsonObject.getJsonObject(key);
            String dataType = attribute.getString("type");
            switch (dataType) {
                case "Long":
                    result.put(key, Long.valueOf(attribute.getJsonNumber("value").toString()));
                    break;
                case "Short":
                    result.put(key, Short.valueOf(attribute.getJsonNumber("value").toString()));
                    break;
                case "Integer":
                    result.put(key, Integer.valueOf(attribute.getJsonNumber("value").toString()));
                    break;
                default:
                    result.put(key, attribute.getString("value"));
                    break;
            }
        }

        //Ergebnis zurueckgeben.
        return result;
    }

}