package de.cherriz.master.businessclient;

import de.cherriz.master.businessclient.dialog.login.LoggedInListener;
import de.cherriz.master.businessclient.dialog.login.LoginController;
import de.cherriz.master.businessclient.dialog.main.MainController;
import de.cherriz.master.businessclient.shared.User;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Startklasse der Anwendung.
 * Erzeugt den Spring Context.
 *
 * @author Frederik Kirsch
 */
@Configuration
public class Starter extends Application implements LoggedInListener {

    private static final String SPRING_CONFIG_PATH = "context.xml";

    private static Stage primaryStage = null;

    private ApplicationContext context = null;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.init(stage);
        this.showLoginDialog();
    }

    @Bean
    public Stage primaryStage() {
        return Starter.primaryStage;
    }

    @Override
    public void loggedIn(User user) {
        this.showMainDialog(user);
    }

    /**
     * Erzeugt den Springcontext. Zusätzlich wird die Stage für Spring zugänglich gemacht.
     *
     * @param stage Die initiale Stage.
     */
    private void init(Stage stage){
        Starter.primaryStage = stage;
        this.context = new ClassPathXmlApplicationContext(SPRING_CONFIG_PATH);
    }

    /**
     * Ruft den LoginDialog auf.
     */
    private void showLoginDialog(){
        LoginController loginController = this.context.getBean(LoginController.class);
        loginController.addLoggedInListener(this);
        loginController.show();
    }

    /**
     * Ruft den Hauptdialog auf.
     */
    private void showMainDialog(User user){
        MainController mainController = this.context.getBean(MainController.class);
        mainController.setActiveUser(user);
        mainController.show();
    }

}