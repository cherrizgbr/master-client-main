package de.cherriz.master.businessclient.services.auftrag;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Einzelnes Attribut eines Auftrags.
 *
 * @author Frederik Kirsch
 */
@JsonIgnoreProperties({"id", "auftragID"})
public class Auftragsdatum {

    /**
     * Der Name.
     */
    @JsonProperty
    public String attribute;

    /**
     * Der Wert.
     */
    @JsonProperty
    public String value;

}