package de.cherriz.master.businessclient.dialog;

import javafx.scene.layout.Pane;

/**
 * Schnittstelle für die UI eines Plugins.
 *
 * @author Frederik Kirsch
 */
public interface PluginView extends View {

    /**
     * Liefert die AnchorPane des Plugins.
     *
     * @return Die AnchorPane.
     */
    Pane getRoot();

}