package de.cherriz.master.businessclient.shared;

import de.cherriz.master.businessclient.dialog.basic.AutowiredPaneController;
import de.cherriz.master.businessclient.services.auftrag.Auftrag;

import java.util.HashMap;
import java.util.Map;

/**
 * Oberklasse aller Plugins. Definiert gemeinsame Funktionen.
 *
 * @author Frederik Kirsch
 */
public abstract class Plugin extends AutowiredPaneController {

    private PluginListener listener = null;

    private Auftrag daten = null;

    private Map<String, String> attributeMap;

    /**
     * Laedt die FXML, instanziiert die UI und bindet das ViewModel an.
     *
     * @param name Der Name des Plugins.
     * @param fxml Der Pfad zum FXML.
     */
    public Plugin(String name, String fxml) {
        this(name, fxml, new String[]{});
    }

    /**
     * Laedt die FXML, instanziiert die UI und bindet das ViewModel an.
     * Die zur Ausfuehrung notwendigen Kontextattribute werden defniert.
     *
     * @param name               Der Name des Der Name des Plugins.
     * @param fxml               Der Pfad zum FXML.
     * @param requiredAttributes Die notwendigen Attribute des ausfuehrenden Kontexts.
     */
    public Plugin(String name, String fxml, String[] requiredAttributes) {
        super(name, fxml);
        this.attributeMap = new HashMap<>();
        for (String attribute : requiredAttributes) {
            this.attributeMap.put(attribute, null);
        }
    }

    /**
     * @return Den ProzessdefinitionKey des unterstuetzten Prozesses.
     */
    public abstract String getProcessDefinitionKey();

    /**
     * @return Der TaskdefinitionKey des unterstuetzen Prozessschrittes.
     */
    public abstract String getTaskDefinitionKey();

    /**
     * @return Die Map in der die notwendigen Kontextattribute hinterlegt sind.
     */
    public Map<String, String> getContextAttributeMap() {
        return this.attributeMap;
    }

    /**
     * Liefert den Wert eines spezifischen Kontextattributes.
     *
     * @param attribute Der Attributname.
     * @return Der Wert.
     */
    protected String getContextAttribute(String attribute) {
        return this.attributeMap.get(attribute);
    }

    /**
     * Startet das Plugin.
     *
     * @param listener Listener für Ergebnisse der Bearbeitung.
     * @param daten Die Auftragsdaten
     */
    public void start(PluginListener listener, Auftrag daten) {
        this.listener = listener;
        this.daten = daten;
        this.startEditing();
    }

    /**
     * Startet die Bearbeitung.
     */
    protected abstract void startEditing();

    /**
     * Liefert den Wert eines spezifischen Auftragsattributs.
     *
     * @param attribute Der Attributname.
     * @return Der Wer.
     */
    protected String getAuftragAttribute(String attribute) {
        return this.daten.getValue(attribute);
    }

    /**
     * Bricht die Bearbeitung ab.
     */
    protected void cancelEditing() {
        this.listener.editingCanceled();
    }

    /**
     * Beendet die Bearbeitung.
     *
     * @param result Praezisiert wie die Bearbeitung beendet wurde.
     */
    protected void finishEdition(String result) {
        this.listener.editingFinished(result);
    }

}