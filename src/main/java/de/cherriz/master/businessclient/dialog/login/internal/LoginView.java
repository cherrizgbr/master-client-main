package de.cherriz.master.businessclient.dialog.login.internal;

import de.cherriz.master.businessclient.dialog.View;
import de.cherriz.master.businessclient.dialog.ViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Viewcontroller für den LoginDialog.
 *
 * @author Frederik Kirsch
 */
public class LoginView implements  View, Initializable, EventHandler<ActionEvent> {

    @FXML
    private TextField txtUser;

    @FXML
    private PasswordField pwdPassword;

    @FXML
    private Label lblError;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnBeenden;

    private LoginViewModel viewModel;

    private final ObjectProperty<EventHandler<ActionEvent>> loginProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> beendenProperty = new SimpleObjectProperty<>();

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
    }

    @Override
    public void setViewModel(final ViewModel viewModel) {
        this.viewModel = viewModel.getCastetModel();

        this.viewModel.userProperty().bind(this.txtUser.textProperty());
        this.viewModel.passwordProperty().bind(this.pwdPassword.textProperty());

        this.lblError.visibleProperty().bind(this.viewModel.loginErrorProperty());

        this.loginProperty.set(this);
        this.btnLogin.onActionProperty().bind(this.loginProperty);

        this.beendenProperty.set(this);
        this.btnBeenden.onActionProperty().bind(this.beendenProperty);
    }

    @Override
    public void handle(ActionEvent event) {
        if(event.getSource().equals(btnLogin)){
            this.viewModel.login();
        }else if(event.getSource().equals(btnBeenden)){
            this.viewModel.beenden();
        }
    }

}