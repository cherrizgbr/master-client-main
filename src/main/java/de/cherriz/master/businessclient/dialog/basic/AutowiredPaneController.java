package de.cherriz.master.businessclient.dialog.basic;

import de.cherriz.master.businessclient.dialog.PluginView;
import de.cherriz.master.businessclient.dialog.ViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * Der Controller für die UI Dialoge lädt die FXML Dateien und instanziiert das entsprechende PluginTab.
 * Zusätzlich wird das ViewModel gegen die UI gebunden.
 * Der Controller arbeitet nach dem MVVM Pattern.
 *
 * @author Frederik Kirsch
 */
public abstract class AutowiredPaneController extends BorderPane {

    private String name = null;

    private ViewModel viewModel = null;

    /**
     * Lädt die FXML und instaziiert den ViewController und bindet das ViewModel an.
     *
     * @param name Der Name des Tabs.
     * @param fxml Der Pfad zur FXML Datei.
     */
    public AutowiredPaneController(String name, String fxml) {
        super();
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(fxml));
        try {
            this.name = name;
            this.viewModel = this.initViewModel();
            loader.load();
            PluginView view = loader.getController();
            view.setViewModel(this.getViewModel());
            this.setCenter(view.getRoot());
            //this.setContent(view.getRoot());
            //this.setText(name);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Erzeugt eine Instanz des ViewModels.
     *
     * @return Das erzeugte ViewModel.
     */
    protected abstract ViewModel initViewModel();

    /**
     * Liefert das spezifische ViewModell der Implementierung zurück.
     *
     * @param <T> Die Implementierung.
     * @return Das spezifische ViewModel.
     */
    protected <T> T getViewModel() {
        return (T) this.viewModel;
    }

    /**
     * @return Der Name des Plugins.
     */
    public String getName() {
        return this.name;
    }

}