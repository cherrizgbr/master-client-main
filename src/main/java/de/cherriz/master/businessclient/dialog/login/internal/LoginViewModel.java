package de.cherriz.master.businessclient.dialog.login.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.shared.User;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Das ViewModel für den UI Dialog.
 * Benachrichtigt über Aktionen in der UI und liefert bei Login zugriff auf das UserObjekt.
 *
 * @author Frederik Kirsch
 */
public class LoginViewModel implements ViewModel {

    private final StringProperty user = new SimpleStringProperty();

    private final StringProperty password = new SimpleStringProperty();

    private BooleanProperty loginError = new SimpleBooleanProperty();

    private List<ValideEventListener<ValideLoginEventType>> listener = null;

    public LoginViewModel() {
        this.listener = new ArrayList<>();
        this.loginError.set(false);
    }

    /**
     * Definiert ob der der Zustand des Logins fehlerhaft ist oder nicht.
     *
     * @param loginError Der Loginzustand.
     */
    public void setLoginError(boolean loginError) {
        this.loginError.set(loginError);
    }

    /**
     * @return Property fuer Fehler beim Login.
     */
    public BooleanProperty loginErrorProperty(){
        return this.loginError;
    }

    /**
     * @return Property fuer den Usernamen.
     */
    public StringProperty userProperty() {
        return this.user;
    }

    /**
     * @return Property fuer das Passwort.
     */
    public StringProperty passwordProperty() {
        return this.password;
    }

    /**
     * @return Das Userobjekt mit den uebergebenen Logindaten.
     */
    public User getUser() {
        return new User(userProperty().get(), passwordProperty().get());
    }

    /**
     * Registriert den uebergebenen EventListener.
     * Der Listener wird ueber valide UIEvents informiert.
     *
     * @param listener Der Listener.
     */
    public void addValideEventListener(ValideEventListener<ValideLoginEventType> listener) {
        this.listener.add(listener);
    }

    /**
     * Informiert die Eventlistener ueber das Klicken des Loginbuttons.
     */
    public void login() {
        this.listener.forEach(listener -> listener.valideEvent(ValideLoginEventType.LOGIN));
    }

    /**
     * Informiert die EventListener ueber das Klicken des BeendenButtons.
     */
    public void beenden() {
        this.listener.forEach(listener -> listener.valideEvent(ValideLoginEventType.BEENDEN));
    }

}