package de.cherriz.master.businessclient.dialog.login.internal;

import de.cherriz.master.businessclient.dialog.basic.ValideEventType;

/**
 * Definiert die Eventtypen fuer den Login Dialog.
 *
 * @author Frederik Kirsch
 */
public enum ValideLoginEventType implements ValideEventType {

    /**
     * Button Login geklickt.
     */
    LOGIN,

    /**
     * Button Beenden geklickt.
     */
    BEENDEN;

}