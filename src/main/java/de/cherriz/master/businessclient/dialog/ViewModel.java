package de.cherriz.master.businessclient.dialog;

/**
 * Schnittstelle für die Implementierung des ViewModels als Teil des MVVM Patterns..
 *
 * @author Frederik Kirsch
 */
public interface ViewModel {

    /**
     * Liefert das spezifische ViewModell der Implementierung zurück.
     *
     * @param <T> Die Implementierung.
     * @return Das spezifische ViewModel.
     */
    public default <T> T getCastetModel() {
        return (T) this;
    }

}