package de.cherriz.master.businessclient.shared;

/**
 * User.
 *
 * @author Frederik Kirsch
 */
public class User {

    private String user = null;

    private String password = null;

    private Boolean loggedIn = Boolean.FALSE;

    /**
     * Konstruktor.
     *
     * @param user     Der Name.
     * @param password Das Passwort.
     */
    public User(String user, String password) {
        this.user = user;
        this.password = password;
    }

    /**
     * @return Der Name des Users.
     */
    public String getUser() {
        return this.user;
    }

    /**
     * @return Das Passwort des Users.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Hinterlegt das der User erfolgreich authentifiziert wurde.
     */
    public void setLoggedIn() {
        this.loggedIn = Boolean.TRUE;
    }

    /**
     * @return Gibt an ob der User erfolgreich authentifiziert wurde.
     */
    public Boolean isLoggedIn() {
        return this.loggedIn;
    }

}