package de.cherriz.master.businessclient.shared;

import de.cherriz.master.businessclient.services.auftrag.Auftrag;
import de.cherriz.master.businessclient.services.auftrag.AuftragService;
import de.cherriz.master.businessclient.services.processes.ProcessInstance;
import de.cherriz.master.businessclient.services.processes.ProcessService;

import java.util.Map;

/**
 * Starter fuer Plugins.
 *
 * @author Frederik Kirsch
 */
public class PluginStarter implements PluginListener {

    //Allgemeine Services
    private ProcessService processService = null;

    private AuftragService auftragService = null;

    //Daten der aktuellen Verarbeitung
    private PluginViewModel viewModel = null;

    private Plugin plugin = null;

    private ProcessInstance process = null;

    private Map<String, Object> processData = null;

    private Auftrag auftrag = null;

    /**
     * Konstruktor.
     *
     * @param processService Service zum lesen von Prozessen.
     * @param auftragService Service zum lesen von Auftragsdaten.
     */
    public PluginStarter(ProcessService processService, AuftragService auftragService) {
        this.processService = processService;
        this.auftragService = auftragService;
    }

    /**
     * Startet das Bearbeiten einer {@link de.cherriz.master.businessclient.services.processes.ProcessInstance}.
     *
     * @param plugin    Das Plugin welches den Prozess bearbeiten soll.
     * @param instance  Die Prozessinstanz welche bearbeitet werden soll.
     * @param viewModel Das VieModel in der das Plugin angezeigt werden soll.
     */
    public void startProcessInstance(Plugin plugin, ProcessInstance instance, PluginViewModel viewModel) {
        //Aktive Verarbeitungsdaten setzen
        this.plugin = plugin;
        this.process = instance;
        this.viewModel = viewModel;

        //Auftragsdaten für Verarbeitung abrufen
        this.processData = this.processService.getProcessAttributes(this.process.getProcessInstanceId());
        Long auftragsdatenID = (Long) this.processData.remove("auftragsdaten");
        this.auftrag = this.auftragService.getProcessAttributes(auftragsdatenID);

        Map<String, String> contextAttributes = this.plugin.getContextAttributeMap();
        for (String key : contextAttributes.keySet()) {
            contextAttributes.put(key, this.processData.get(key).toString());
        }

        this.viewModel.setActivePlugin(this.plugin);
        plugin.start(this, auftrag);
    }

    @Override
    public void editingCanceled() {
        this.viewModel.setActivePlugin(null);

        this.resetData();
    }

    @Override
    public void editingFinished(String result) {
        this.viewModel.setActivePlugin(null);
        this.viewModel.removeProcess(this.process);

        this.processService.setProcessAttribute(this.process.getProcessInstanceId(), "taskResult", result);
        this.processService.dispatchProcess(this.process.getId());

        this.resetData();
    }

    /**
     * Setzt das Plugin nach dem Ende der Bearbeitung zurueck.
     */
    private void resetData() {
        this.plugin = null;
        this.process = null;
        this.viewModel = null;
        this.auftrag = null;
        this.processData = null;
    }

}