package de.cherriz.master.businessclient.dialog.basic;

import de.cherriz.master.businessclient.dialog.View;
import de.cherriz.master.businessclient.dialog.ViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Der Controller dient dem laden der fxml Dateien für die UI Dialoge und instanziiert den entsprechenden Dialog.
 * Zusätzlich wird das ViewModel gegen die UI gebunden.
 * Der Controller ist nach dem MVVM Pattern aufgebaut.
 *
 * @author Frederik Kirsch
 */
public abstract class AutowiredDialogController extends Stage {

    private ViewModel viewModel = null;

    /**
     * Lädt die FXML und instaziiert den ViewController und bindet das ViewModel an.
     *
     * @param fxml  Der Pfad zur FXML Datei.
     * @param owner Der Besitzer des Dialogs. Im Regelfall die Stage des BusinessClient.
     */
    public AutowiredDialogController(String fxml, Window owner) {
        super(StageStyle.DECORATED);
        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(fxml));
        try {
            setScene(new Scene(loader.load()));
            View view = loader.getController();
            this.viewModel = this.initViewModel();
            view.setViewModel(this.getViewModel());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Erzeugt eine Instanz des ViewModels.
     *
     * @return Das erzeugte ViewModel.
     */
    protected abstract ViewModel initViewModel();

    /**
     * Liefert das spezifische ViewModell der Implementierung zurück.
     *
     * @param <T> Die Implementierung.
     * @return Das spezifische ViewModel.
     */
    protected <T> T getViewModel() {
        return (T) this.viewModel;
    }

}