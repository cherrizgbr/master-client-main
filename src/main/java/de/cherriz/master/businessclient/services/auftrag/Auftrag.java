package de.cherriz.master.businessclient.services.auftrag;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Auftrag enthaelt Daten zur Verarbeitung.
 *
 * @author Frederik Kirsch
 */
public class Auftrag {

    private Long id = null;

    private String prozess = null;

    private Map<String, String> daten = null;

    /**
     * Setzt die ID des Auftrags.
     *
     * @param id Die AuftragsID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Setzt den Prozess, durch den der Auftrag verarbeitet werden soll.
     *
     * @param prozess Der Prozess.
     */
    public void setProzess(String prozess) {
        this.prozess = prozess;
    }

    /**
     * Setzt die Auftragsdaten die verarbeitet werden sollen.
     *
     * @param auftragsDaten Die Auftragsdaten.
     */
    public void setAuftragsDaten(List<Auftragsdatum> auftragsDaten){
        this.daten = new HashMap<>();
        auftragsDaten.forEach(p -> this.daten.put(p.attribute, p.value));
    }

    /**
     * @return Die ID des Auftrags..
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @return Der Prozess der den Auftrag ueberarbeiten soll.
     */
    public String getProzess() {
        return this.prozess;
    }

    /**
     * Liefert den Wert für ein Attribut des Auftrags.
     *
     * @param attribute Das Attribut.
     * @return Der Wert des Attributs.
     */
    public String getValue(String attribute) {
        return this.daten.get(attribute);
    }

}