package de.cherriz.master.businessclient.services.processes.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.businessclient.services.processes.ProcessInstance;
import de.cherriz.master.businessclient.services.processes.ProcessService;
import de.cherriz.master.businessclient.services.processes.StepDescription;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementierung des {@link de.cherriz.master.businessclient.services.processes.ProcessService}.
 *
 * @author Frederik Kirsch
 */
public class ProcessServiceImpl extends AbstractService implements ProcessService {

    private String taskPath = null;

    private String processVariablePath = null;

    private String completePath = null;

    public ProcessServiceImpl(){
        this.addMessageBodyReader(AttributeMessageBodyReader.class);
    }

    /**
     * Setzt den Pfad zur Funktion Prozessinstanzen zu TaskKey.
     *
     * @param taskPath Der Pfad.
     */
    public void setTaskPath(String taskPath) {
        this.taskPath = taskPath;
    }

    /**
     * Setzt den Pfad zur Funktion Prozessattribute zu ID.
     *
     * @param processVariablePath Der Pfad.
     */
    public void setProcessVariablePath(String processVariablePath) {
        this.processVariablePath = processVariablePath;
    }

    /**
     * Setzt den Pfad zur Funktion Prozessschritt Abschließen.
     *
     * @param completePath Der Pfad.
     */
    public void setCompletePath(String completePath) {
        this.completePath = completePath;
    }

    @Override
    public List<ProcessInstance> getProcessInstances(List<StepDescription> processSteps) {
        List<ProcessInstance> result = new ArrayList<>();

        //Request absetzen
        for (StepDescription processStep : processSteps) {
            String resultString = this.getTarget(this.taskPath).queryParam("processDefinitionKey", processStep.getProcessDefinitionKey())
                    .queryParam("taskDefinitionKey", processStep.getTaskDefinitionKey())
                    .request(MediaType.APPLICATION_JSON_TYPE).get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            try {
                List<ProcessInstance> response = mapper.readValue(resultString, new TypeReference<List<ProcessInstance>>() {
                });
                response.forEach(p -> p.setProcessDefinitionKey(processStep.getProcessDefinitionKey()));
                result.addAll(response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public Map<String, Object> getProcessAttributes(String processID) {
        return this.getTarget(this.processVariablePath.replace("{id}", processID))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<Map<String, Object>>() {
                });
    }

    @Override
    public void setProcessAttribute(String processID, String attribute, String value) {
        JsonObject request = Json.createObjectBuilder().add("modifications", Json.createObjectBuilder().add(attribute, Json.createObjectBuilder().add("value", value))).build();
        this.getTarget(this.processVariablePath.replace("{id}", processID)).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(request.toString()), String.class);
    }

    @Override
    public void dispatchProcess(String processID) {
        JsonObject request = Json.createObjectBuilder().build();
        this.getTarget(this.completePath.replace("{id}", processID)).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(request.toString()), String.class);
    }

}