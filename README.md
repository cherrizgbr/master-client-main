# master-client-main #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Der Java Client repräsentiert das Back Office System eines Unternehmens. Es handelt sich um eine Anwendung mit Frameworkcharakter. Das Programm sucht in der Process Engine nach zu bearbeitenden Prozessen. Die eigentliche Bearbeitung von Prozessschritten findet durch Plugins statt. Die Plugins definieren welche Prozesse sie bearbeiten können.
![Business Client](http://www.cherriz.de/master/images/content/business_client.png "Business Client")

### Weiterführende Übersicht ###

Dieser Client ist die Basis des im Architekturschaubild dargestellten Business Clients. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


