package de.cherriz.master.businessclient.dialog.main;

import de.cherriz.master.businessclient.dialog.DialogController;
import de.cherriz.master.businessclient.shared.User;

/**
 * Controller für den Hauptdialog des BusinessClients.
 *
 * @author Frederik Kirsch
 */
public interface MainController extends DialogController {

    /**
     * Setzt den aktiven User.
     *
     * @param user Der User.
     */
    void setActiveUser(User user);

}