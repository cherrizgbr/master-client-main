package de.cherriz.master.businessclient.dialog.main.internal;

import de.cherriz.master.businessclient.dialog.View;
import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.shared.Plugin;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * ViewController des Hauptfensters des BusinessClients.
 *
 * @author Frederik Kirsch
 */
public class MainView implements View, EventHandler<ActionEvent>, ChangeListener<Plugin> {

    @FXML
    private ListView<String> lstPlugins;

    @FXML
    private Label lblVersion;

    @FXML
    private Label lblBuild;

    @FXML
    private Label lblUser;

    @FXML
    private PostkorbView vwPostkorbController;

    @FXML
    private TabPane tbpContent;

    private MainViewModel viewModel;

    private final ObjectProperty<EventHandler<ActionEvent>> aktualisierenProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> bearbeitenProperty = new SimpleObjectProperty<>();

    private ObjectProperty<Plugin> plugin = new SimpleObjectProperty<>();

    private Tab activeTab = null;

    @Override
    public void setViewModel(final ViewModel viewModel) {
        this.viewModel = viewModel.getCastetModel();
        this.lblVersion.textProperty().bind(Bindings.concat("Build-Time: ").concat(this.viewModel.buildProperty()));
        this.lblBuild.textProperty().bind(Bindings.concat("Version: ").concat(this.viewModel.versionProperty()));
        this.lblUser.textProperty().bind(Bindings.concat("User: ").concat(this.viewModel.userProperty()));
        this.lstPlugins.itemsProperty().bind(this.viewModel.pluginsProperty());

        //Tabelle
        this.vwPostkorbController.getClmId().setCellValueFactory(new PropertyValueFactory<>("processInstanceId"));
        this.vwPostkorbController.getClmProzess().setCellValueFactory(new PropertyValueFactory<>("name"));
        this.vwPostkorbController.getClmDatum().setCellValueFactory(new PropertyValueFactory<>("created"));

        this.vwPostkorbController.getTblVorgaenge().getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.vwPostkorbController.getTblVorgaenge().itemsProperty().bind(this.viewModel.processInstancesProperty());
        this.vwPostkorbController.getBtnBearbeiten().disableProperty().bind(this.vwPostkorbController.getTblVorgaenge().getSelectionModel().selectedItemProperty().isNull());
        this.viewModel.selectedProcessProperty().bind(this.vwPostkorbController.getTblVorgaenge().getSelectionModel().selectedItemProperty());

        //Buttons
        this.aktualisierenProperty.set(this);
        this.vwPostkorbController.getBtnAktualisieren().onActionProperty().bind(this.aktualisierenProperty);

        this.bearbeitenProperty.set(this);
        this.vwPostkorbController.getBtnBearbeiten().onActionProperty().bind(this.bearbeitenProperty);

        //Plugin
        this.plugin.bind(this.viewModel.activePluginProperty());
        this.plugin.addListener(this);
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource().equals(this.vwPostkorbController.getBtnAktualisieren())) {
            this.viewModel.aktualisieren();
        } else if (event.getSource().equals(this.vwPostkorbController.getBtnBearbeiten())) {
            this.viewModel.bearbeiten();
        }
    }

    @Override
    public void changed(ObservableValue<? extends Plugin> observable, Plugin oldValue, Plugin newValue) {
        if (newValue == null) {
            if (this.tbpContent.getTabs().size() > 1) {
                this.tbpContent.getTabs().remove(this.activeTab);
                this.activeTab = null;
            }

            this.tbpContent.getSelectionModel().select(0);
            this.tbpContent.getTabs().get(0).setDisable(false);
        } else {
            this.activeTab = new Tab();
            this.activeTab.setContent(newValue);
            this.activeTab.setText(newValue.getName());

            this.tbpContent.getTabs().add(this.activeTab);
            this.tbpContent.getSelectionModel().select(this.activeTab);

            this.tbpContent.getTabs().get(0).setDisable(true);
        }
    }

}