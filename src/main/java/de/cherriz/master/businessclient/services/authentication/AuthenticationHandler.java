package de.cherriz.master.businessclient.services.authentication;

import de.cherriz.master.businessclient.shared.User;

/**
 * Service zur Authentifizierung von Logininformationen.
 *
 * @author Frederik Kirsch
 */
public interface AuthenticationHandler {

    /**
     * Authentifiziert den uebergebenen User.
     * Setzt dessen Status und gibt das modifizierte Objekt als Ergebnis zurueck.
     *
     * @param user Der zu authentifizierende User.
     * @return Der authentifizierte User.
     */
    public User authenticate(User user);

}
