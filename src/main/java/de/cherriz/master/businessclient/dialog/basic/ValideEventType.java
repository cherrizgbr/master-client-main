package de.cherriz.master.businessclient.dialog.basic;

/**
 * Schnittstelle definiert das die ableitende Enum valide Eventtypen definiert.
 *
 * @author Frederik Kirsch
 */
public interface ValideEventType {}