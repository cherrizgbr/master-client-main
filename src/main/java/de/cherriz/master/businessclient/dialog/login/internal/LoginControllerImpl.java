package de.cherriz.master.businessclient.dialog.login.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.AutowiredDialogController;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.dialog.login.LoggedInListener;
import de.cherriz.master.businessclient.dialog.login.LoginController;
import de.cherriz.master.businessclient.services.authentication.AuthenticationHandler;
import de.cherriz.master.businessclient.shared.User;
import javafx.stage.Window;

import java.util.ArrayList;
import java.util.List;

/**
 * Die Implementierung des Logincontrollers.
 *
 * @author Frederik Kirsch
 */
public class LoginControllerImpl extends AutowiredDialogController implements LoginController, ValideEventListener<ValideLoginEventType> {

    private List<LoggedInListener> listener = null;

    private AuthenticationHandler authHandler = null;

    /**
     * Instanziiert den Controller und setzt die Notwendigen Daten.
     *
     * @param fxml        Die FXML Datei des Dialogs.
     * @param name        Der Name des Dialogs.
     * @param owner       Der Besitzer des Dialogs.
     * @param authHandler Der Authentication Handler der den Login übernimmt.
     */
    public LoginControllerImpl(String fxml, String name, Window owner, AuthenticationHandler authHandler) {
        super(fxml, owner);
        this.authHandler = authHandler;
        this.setTitle(name);

        this.setMinHeight(210);
        this.setMinWidth(475);

        this.setResizable(false);
        this.listener = new ArrayList<>();
    }

    @Override
    protected ViewModel initViewModel() {
        LoginViewModel viewModel = new LoginViewModel();
        viewModel.addValideEventListener(this);
        return viewModel;
    }

    @Override
    public void addLoggedInListener(LoggedInListener listener) {
        this.listener.add(listener);
    }

    @Override
    public void valideEvent(ValideLoginEventType type) {
        switch (type) {
            case LOGIN:
                this.handleLogin();
                break;
            case BEENDEN:
                this.hide();
                break;
        }
    }

    /**
     * Fuehrt die Authentifizierung mit den eingegebenen Informationen durch.
     * Bei erfolgreichem Login werden die Listener informiert.
     */
    private void handleLogin() {
        LoginViewModel viewModel = this.getViewModel();
        User user = this.authHandler.authenticate(viewModel.getUser());

        if (user.isLoggedIn()) {
            this.listener.forEach(listener -> listener.loggedIn(user));
            viewModel.setLoginError(false);
            this.hide();
        }else{
            viewModel.setLoginError(true);
        }
    }

}